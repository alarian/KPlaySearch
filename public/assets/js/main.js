// Long term cookies
Cookies.defaults = { expires: 365 };

// Animated loading dots
setInterval(() => document.querySelectorAll('.loading-dots').forEach(el => el.textContent.length < 3 ? el.textContent += '.' : el.textContent = ''), 500);


/*
Dark mode
 */

const darkButton = document.querySelector('.dark');

const toggleDark = () => {
	// Toggle UIkit properties
	['uk-light', 'uk-background-secondary'].forEach(Class => [document.body, document.querySelector('.uk-modal-body')].forEach(el => el.classList.toggle(Class)));
	// Toggle dark button text
	darkButton.textContent = darkButton.textContent === 'Dark' ? 'White' : 'Dark';
};

darkButton.onclick = () => {
	// Save white/dark state
	if(Cookies.get('dark') === 'true'){
		Cookies.set('dark', 'false');
	}
	else {
		Cookies.set('dark', 'true');
	}
	// Toggle
	toggleDark();
};

// Restore saved dark style
if(Cookies.get('dark') === 'true') toggleDark();
