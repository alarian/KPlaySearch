const GPlay = require('google-play-scraper');

module.exports = {
    /*
    Google Play search using scraper API
     */
    search: async query => {
        const res = await GPlay.search({ term: query.name, num: query.number, price: query.price });
        for(let i = 0; i < res.length; i++){
            Object.assign(res[i], {
                index: i,
                ... (await GPlay.app({ appId: res[i].appId })),
                ... { permissions: (await GPlay.permissions({ appId: res[i].appId })) }
            })
        }
        return res;
    }
};
